<?php
$page = 1;
if (!isset($_GET['page'])) {
    $page = $_GET['page'];
}
$data = [
    1 => [
        'One',
        'Two',
        'Three',
    ],
    2 => [
        'Four',
        'Five',
        'Six',
    ],
    3 => [
        'Seven',
        'Eight',
        'Nine',
    ],
];
?>
<div class="plg-demo_pluginD">
    <?php foreach ($data[$page] as $item): ?>
        <div><?= $item ?></div>
    <?php endforeach; ?>
    <div>
        <?php foreach (array_keys($data) as $page): ?>
            <button class="plg-demo_pluginD-page"><?= $page ?></button>
        <?php endforeach; ?>
    </div>
</div>

/*
 * (c) Mike Shapovalov <smikebox@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
(function ($) {
    jc.namespace('jc');
    
    /**
     * Plugin constructor
     * @param {jQuery} el an element that connects the plugin
     * @param {Object} opt plugin
     * @constructor
     */
    jc.Plugin = function (el, opt) {
        if (!opt) {
            opt = {};
        }
        
        var self = this;
        
        //Gets plugin options from attribute
        var optFromAttr = el.attr('data-' + self.name + '-opt');
        if(!optFromAttr){
            optFromAttr = el.attr('data-opt');
        }
        
        
        //If attribute has options
        if (optFromAttr) {
            opt = $.extend(true, {}, $.parseJSON(optFromAttr), opt);
        }
        else{
            opt = el.data();
        }
        
        self.cfg = $.extend(true, {}, self.cfg, opt);
        
        
        //Protect options from changes
        self.opt = function () {
            return opt;
        };
        
        //Protect element from changes
        self.el = function () {
            return el;
        };
        
        self.defineCfg();
        
    };
    
    var constructor = jc.makeConstructor(jc.Plugin, 'jc.Plugin', Object);
    var prototype = constructor.prototype;
    
    /**
     * Name of plugin
     *
     * @type {string}
     */
    prototype.name = null;
    
    /**
     * Default options
     *
     * @type {Object}
     */
    prototype.defaults = {};
    
    /**
     * Default source code
     *
     * @type {string}
     */
    prototype.source = '<div></div>';
    
    /**
     * Configuration
     * @type {Object}
     */
    prototype.cfg = {
        on: {
            init: function () {
                
            }
        }
    };
    
    
    /**
     * Runs on plugin attaches to elements
     */
    prototype.init = function () {
        var self = this;
        
        if (!self.cfg.on) {
            self.cfg.on = {};
        }
        
        $.each(self.cfg.on, function (eventName, handlers) {
            
            if (handlers instanceof Function) {
                self.cfg.on[eventName] = [handlers];
            }
        });
        
        jc.attachPlugins(self.el());
        self.fireEvent('init');
    };
    
    /**
     * Returns the element that was connected to the plug-in
     */
    prototype.el = function () {
    };
    
    /**
     * Returns plugin initialization options
     */
    prototype.opt = function () {
    };
    
    /**
     * Returns plugin child-element
     *
     * @param {string} name name of element ()
     *
     * @returns {jQuery}
     */
    prototype.find = function (name) {
        var self = this;
        return self.el().find(self.makeSelector(name));
    };
    
    /**
     * Makes selector for plugin child-element
     *
     * @param {string} name element class name suffix e.g. button for plg-demo_Grid-button)
     *
     * @returns {string}
     */
    prototype.makeSelector = function (name) {
        var self = this;
        return '.' + self.makeCssClass() + '-' + name;
    };
    
    /**
     * Makes plugin css class
     *
     * @return {string}
     */
    prototype.makeCssClass = function () {
        var self = this;
        return 'plg-' + self.name;
    };
    
    
    /**
     * Makes plugin default configuration
     *
     * @param {Object} defaults
     */
    prototype.defineCfg = function (defaults) {
        var self = this;
        if (!defaults) {
            defaults = {};
        }
        self.cfg = $.extend(true, {}, self.defaults, defaults, self.cfg);
    };
    
    /**
     * fire jquery event
     *
     * @param {string} eventName
     * @param {object} params
     *
     * @return {jQuery}
     */
    prototype.trigger = function (eventName, params) {
        var self = this;
        self.el().trigger(self.name + '.' + eventName, params);
        return self.el();
    };
    
    /**
     * fire plugin event
     *
     * @param {string} eventName
     * @param {object} params
     *
     * @return {jQuery}
     */
    prototype.fireEvent = function (eventName, params) {
        var self = this;
        
        if (self.cfg.on[eventName]) {
            $.each(self.cfg.on[eventName], function (k, handler) {
                handler.call(self, params);
            });
        }
        return self.el();
    };
    
    /**
     * Attach event handler
     *
     * @param {string} eventName
     * @param {function} handler
     *
     * @return {Object}
     */
    prototype.on = function (eventName, handler) {
        var self = this;
        if (!self.cfg.on[eventName]) {
            self.cfg.on[eventName] = [];
        }
        else if (!Array.isArray(self.cfg.on[eventName])) {
            self.cfg.on[eventName] = [self.cfg.on[eventName]];
        }
        self.cfg.on[eventName].push(handler)
        return self;
    };
    
    /**
     * Sets plugin html source
     *
     * @param {string|Object} source html-code of ajax config to get source from server
     * @param {bool|string} extend extend current source (true, false, 'recursive')
     *
     * @return {void}
     */
    prototype.setSource = function (source, extend) {
        var self = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if ((source instanceof Object) && extend) {
            self.source = $.extend(recursive, {}, self.source, source);
        }
        else {
            self.source = source
        }
    };
    
    /**
     * Sets plugin configuration
     *
     * @param {Object} cfg
     * @param {bool|string} extend extend current configuration (true, false, 'recursive')
     */
    prototype.setCfg = function (cfg, extend) {
        var self = this;
        var recursive = false;
        if (extend == 'recursive') {
            recursive = true;
        }
        if (extend) {
            self.cfg = $.extend(recursive, {}, self.cfg, cfg);
        }
        else {
            self.cfg = cfg;
        }
    };
    
    /**
     * Gets plugin html source
     * @param {function} handler
     * @param {Object|string|null} extendedSource ajax query config or html
     */
    prototype.getSource = function (handler, extendedSource) {
        var self = this;
        
        if ((self.source instanceof Object) || (extendedSource instanceof Object)) {
            if (!extendedSource) {
                extendedSource = {};
            }
            
            if (self.source instanceof Object) {
                extendedSource = $.extend(true, {}, self.source, extendedSource)
            }
            
            var success = extendedSource.success;
            extendedSource.success = function (data) {
                if (success) {
                    success.call(this, data);
                }
                handler.call(self, data);
                
            };
            $.ajax(extendedSource);
        }
        else {
            if (!extendedSource) {
                extendedSource = self.source;
            }
            if (!extendedSource) {
                throw new Error('No plugin source!')
            }
            handler.call(self, extendedSource);
        }
    };
    
    /**
     * Reloads plugin (reload html and init plugin)
     *
     * @param {Object} p
     *      {Object} opt plugin options
     *      {string|Object} source html code or ajax config
     *      {bool} extendCurrentSource true false 'recursive'
     * @param handler
     */
    prototype.reload = function (p, handler) {
        
        var self = this;
        
        var def = {
            source: self.source,
            extendCurrentSource: 'recursive',
            extendOpt: 'recursive'
        };
        
        p = $.extend(true, {}, def, p);
        
        
        self.getSource(function (data) {
        
            self.el().html($(data).html());
            
            if (p.source) {
                self.setSource(p.source, p.extendCurrentSource)
            }
            
            if (p.opt) {
                self.setCfg(p.opt, p.extendOpt);
            }
            
            self.init();
            
            jc.attachPlugins(self.el());
            
            if (handler) {
                handler.call(self);
            }
            
        }, p.source)
    };
    
    
})(jQuery);